# Create Your Own Satellite!
    
Want to (kinda) build your own satellite in 7 days?
You will:

    - Use hardware of your choice Arduino, RaspberryPi or whatever else you have at your disposal.

    - Interact with the “space” environment through mock inputs to your MCU or connect sensors 
    and take measurements (inside and outside temperature, sun position, angular velocity) or 
    even pictures. Bonus points (in coolness) if you use actual external devices.

    - Make a local server simulating the ground station and request information from 
    your satellite!

And if you’d like to try something else, **sky is (literally) the limit** !

Upload your work as such: your code, plus video presentation, plus small how-to use document.

Don't hesitate to send us questions here: `acubesat.obc@spacedot.gr`

# Create A Space Mission Control Interface

Have you watched rocket launches? Have you seen the huge screens of NASA Mission Control Centers? Do you want to make one yourself?

In this challenge, you can create a user interface that shows:

    - The satellite’s location above the earth.
    - The satellite’s health values such as temperature, voltage, etc.
    - A list of telecommand/telemetry messages.
    - A list of events happening on the satellite.

You should try to make it usable, yet beautiful!


Feel free to use any technology stack, tool or utility you’re most familiar with or prefer! There are no limits (except your imagination). Make it a mobile app, a web page, a desktop program, you name it!

Hints: Use ideas from Mission Control software available online, from Google, from ESA/NASA/JAXA missions, or even from your favourite sci-fi show!

**Make sure your submitted work can be used and explored by our team** , for instance downloadable app, with how-to set up and operate instructions.

Don't hesitate to send us questions here: `acubesat.obc@spacedot.gr`
