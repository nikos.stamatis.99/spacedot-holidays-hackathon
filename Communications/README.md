### Someone broke in SpaceDot’s lab two days ago. SpaceDotians just received a message with some clues that may allow them solve the mystery.. 

## Help us out!

**[Challenge 1]**

Are you up to the task to get the bigger picture and find out what is missing from the lab?

**[Challenge 2]** 

Even Gauss himself couldn't crack the code and figure out who took the missing object.


**[Challenge 3]**

A mysterious sequence repeats itself in this signal. Maybe this could be the key behind where the stolen posessions are located.

**[Challenge 4]** 

The thief dropped a note with an illegible message on it: `áÝÐ\x95ÖÀ×ÐÆÔÁ\x95ÜÆ\x95ÔÁ\x95öæó`.

Decrypt the signal to recover the original message and find the hidden location.

**[Challenge 5]** 

Can you decode the blinking lights to find out what time the thieves broke into the lab?

If you manage to crack at least one of the challenges, you can send your solutions to `acubesat.comms@spacedot.gr` until 20/12/2021!

<br />

Challenge | #1 | #2 | #3 | #4 | #5
--- | --- | --- | --- |--- |---
Difficulty | ⭐ ⭐ | ⭐ ⭐ ⭐ ⭐ | ⭐ ⭐ ⭐ | ⭐ ⭐ ⭐ | ⭐ 

<br />

# General Infromation

Each challenge has a corresponding file in this repo that is needed for it. Higher-difficulty challenges assume background knowledge in DSP (e.g. digital modulation schemes) and those marked as a lower difficulty are more basic and only need things that should be familiar to any enthusiast. Visualizing the signal is pivotal to most challenges here so try to play around as much as possible!

**Challenge 1** becomes far easier with some DSP-related software (e.g. GNURadio), but once you figure out what you have to do the challenge becomes almost trivial and requires minimal work on your part.

**Challenge 2** requires something like GNURadio or the DSP toolbox for MATLAB. The data contained in the binary file is encoded as `complex64`.

**Challenge 3** you could try this out with GNURadio or related software but the easiest way would be to use Python's `SciPy` library or the equivalent to whatever your favourite scripting programming language is. The data contained in the binary file should be read as `float64`.

**Challenge 4** this again requires minimal software and the functionality you need should be supported (and if not it should be fairly easy to code up yourself). Because the file is a simple `.txt`, the easiest way to approach this would probably be by programming. The difficulty here is that you need some out-of-the-box thinking to see how the data given is connected to the illegible message ;) 

**Challenge 5** can be solved without any special software in mind although it is far easier to solve with a simple scripting language (e.g. Python).

**More hints** will be posted in the following days so be sure to stay tuned

